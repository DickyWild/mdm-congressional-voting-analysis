In this folder, we have code and figures used for the support of a report on the groupings existing within the US Congress.

The Excel Macro formats the data, imported with each field as a text field

The MATLAB and R code take the output from the Excel Macro, saved as an .xlsx file

The MATLAB code performs spectral clustering

The R code performs Hierarchical clustering, MCA, and k-modes
