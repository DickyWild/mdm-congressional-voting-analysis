function [final2] = HouseRoll(a,b,c,d,SENATORS)
%This function creates 2 correaltions matrices, linear and machine learning and
%performs Spectral Clustering on:
%voting matrix (a)
%Reduce to (b) dimensions
%into (c) clusters
%(d) controls controls normalisation of data (1,2 or 3)
% SENATORS is the information on members
%How to use:
%Using macro uploaded on senate/house data import all votes in numeric
%matrix form (a)
%Import other 3 columns of information as string array (SENATORS)
%Programme will output table of clusters and save as text file ready for
%further analysis in Cytoscape
n = size(a);
N1 = n(1);
N2 = n(2);
Testing_Matrix = zeros(N1,N2);
Corr = ones(N1,N1);
Corrr = Corr./2;
Pointless = ones(N1,1);
Point = diag(Pointless);
point2 = Point./2;
Correlation_Matrix2 = Corrr + point2;
%creates estimate correlation matrix for machine learning
Agree_Matrix = zeros(N1,N1);
Disagree_Matrix = zeros(N1,N1);
for k = 1:N1
    for i = 1:N2
        for j = 1:N1
            if a(j,i) == 1 || a(j,i) == 6
                if a(k,i) == 1 || a(k,i) == 6
                    Testing_Matrix(j,i) = a(j,i) == a(k,i);
                    if Testing_Matrix(j,i) == Correlation_Matrix2(j,k)
                        Correlation_Matrix2(j,k) = Correlation_Matrix2(j,k);
                    elseif Testing_Matrix(j,i) < Correlation_Matrix2(j,k)
                        Correlation_Matrix2(j,k) = 0.95*Correlation_Matrix2(j,k);
                    else
                        Correlation_Matrix2(j,k) = min([1.05*Correlation_Matrix2(j,k),1]);                   
                    end
                    if Testing_Matrix(j,i) == 1
                        Agree_Matrix(j,k) = Agree_Matrix(j,k) + 1;
                    end
                end
            end
        end
    end
end
%creates machine learning correlation matrix
Correlation_Matrix3 = [];
j=1;
for i = 1:N1
    if Agree_Matrix(i,i) > N2/2
        Correlation_Matrix3(j)=i;
        j=j+1;
    end
end
correlation_matrix4 = Correlation_Matrix2(Correlation_Matrix3,Correlation_Matrix3);
%removes members who don't vote enough
for i = 1:N1
    for j = 1:N1
        Agreement_Matrix(i,j) = Agree_Matrix(i,j)/min(Agree_Matrix(i,i),Agree_Matrix(j,j));
        Disagreement_Matrix(i,j) = Disagree_Matrix(i,j)/min(Agree_Matrix(i,i),Agree_Matrix(j,j));
    end
end
Agreement_Matrix = Agreement_Matrix(Correlation_Matrix3,Correlation_Matrix3);
%creates Correlation matrix


  heatmap(Agreement_Matrix)
  title('Correlation Matrix')
  xlabel('Senators')
  ylabel('Senators')
  


compare = array2table(SENATORS(Correlation_Matrix3,:))
n = size(compare);
N1 = n(1);
Ernisty = SpectralClustering(Agreement_Matrix,b,c,d);
final  = [compare Ernisty];
final.Properties.VariableNames = {'State' 'Party' 'Name' 'Group'};
final2 = sortrows(final,4);
%performs spectral clustering

writetable(final2,'tableforplots');
%saves as text file for Cytoscape